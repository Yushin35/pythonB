# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

# Create your models here.


class BlogPost(models.Model):
    class Meta:
        ordering = ('-timestamp',)

    id = models.AutoField(primary_key=True)
    author = models.CharField(max_length=150, null=True)
    theme = models.CharField(max_length=150, null=True)
    title = models.CharField(max_length=150, null=True)
    body = models.TextField(null=True)
    image = models.ImageField(null=True)
    views = models.IntegerField(null=True)
    comments = models.IntegerField(null=True)
    timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'id->%s title->%s'%(self.id, self.title)

