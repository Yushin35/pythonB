# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from main.models import BlogPost
from django.contrib import admin

# Register your models here.
admin.site.register(BlogPost)