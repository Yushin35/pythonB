function selfRandom(min, max)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function generateSvgCircle(count, radius, windowWidth, windowHeight) {
    var circles = [];
    for(var i = 0; i < count; i++) {
        var ccircle = {
            el: undefined,
            id: "head_circle_" + i.toString(),
            cx: selfRandom(0 + radius, windowWidth - radius),
            cy: selfRandom(0 + radius, windowHeight - radius),
            r: selfRandom(5, radius),
            stroke:'darkseagreen',
            stroke_width:'1',
            fill: 'grey',
            fill_opacity: '0.3',
            windowWidth: windowWidth,
            windowHeight: windowHeight,
            directionX: 1,
            directionY: 1,
            absc: 1,
            ordin: 1,
            move: function() {
                var cx = parseInt(this.el.getAttribute('cx'));
                var cy = parseInt(this.el.getAttribute('cy'));
                if(cx + this.r >= this.windowWidth) {
                    this.directionX = -1;
                }
                if(cx - this.r <= 0 ) {
                    this.directionX = 1;
                }
                if(cy + this.r >= this.windowHeight) {
                    this.directionY = -1;
                }
                if(cy - this.r <= 0 ) {
                    this.directionY = 1;
                }
                if(this.directionX == 0) {
                    this.directionX = 1;
                }
                if(this.directionY == 0) {
                    this.directionY = 1;
                }
                this.el.setAttribute('cx', (cx + this.absc*this.directionX + selfRandom(-1, 2)).toString());
                this.el.setAttribute('cy', (cy + this.ordin*this.directionY + selfRandom(1, -2)).toString());
            },
            toString: function () {
                return "<circle id=" + '"'+ this.id + '"'  +" "+
                    "cx="+'"'+this.cx+'"'+
                    'cy='+ '"'+ this.cy +'"' +
                    'r='+ '"'+ this.r +'"' +
                    'stroke='+ '"'+ this.stroke +'"' +
                    'stroke-width='+ '\"'+ this.stroke_width +'"' +
                    'fill='+ '"'+ this.fill+'"' +
                    'fill-opacity='+ '"'+ this.fill_opacity +'"' +
                    '></circle>'
            }
        };
        circles.push(ccircle);
    }
    return circles;
}/*
function getCirclesElement(circleObj) {
    for(var i = 0; i<circleObj.length; i++) {

    }
}*/

function distance(x1, y1, x2, y2) {
    return Math.sqrt((x1-x2)*(x1-x2) + (y1 - y2)*(y1 - y2));
}
function moveCircles(circles) {
    for(var i = 0; i < circles.length; i++) {
        for(var j = i+1; j<circles.length; j++) {
            var r1 = circles[i].r;
            var r2 = circles[j].r;
            var x1 = parseInt(circles[i].el.getAttribute('cx'));
            var y1 = parseInt(circles[i].el.getAttribute('cy'));

            var x2 = parseInt(circles[j].el.getAttribute('cx'));
            var y2 = parseInt(circles[j].el.getAttribute('cy'));
            var d = distance(x1, y1, x2, y2);
            /*
            if((d - r1 -r2) <=0) {
                console.log("d=>"+d);
                console.log("x1=>"+x1);
                console.log("x2=>"+x2);
                console.log("y1=>"+y1);
                console.log("y2=>"+y2);
                console.log("r1=>"+r1);
                console.log("r2=>"+r2);
                console.log("id1=>"+circles[i].el.id);
                console.log("id2=>"+circles[j].el.id);
                alert();
            }*/
        }
        circles[i].move();
    }
}
function init() {
    var header_main_svg = document.getElementById('header_main_svg');
    var circlesObj = generateSvgCircle(COUNT, SIZE, 1100, 150);
    //console.log(c);
    console.log(header_main_svg.width);
    for(var i = 0; i<circlesObj.length; i++) {
        header_main_svg.innerHTML += circlesObj[i].toString();
    }
    ///особенность сразу не получаеться
    //получить в одном цыкле после добавления элемента на него ссылку
    for(var i = 0; i<circlesObj.length; i++) {
        circlesObj[i].el = document.getElementById(circlesObj[i].id);
    }
    var circleIntervalId = setInterval(moveCircles, MS, circlesObj);
}
COUNT = 7;
SIZE = 25;
MS = 5;
//window.addEventListener('load', init, false);
