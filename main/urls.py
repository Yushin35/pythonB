from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^blog/$', views.blog, name='blog'),
    url(r'^forum/$', views.forum, name='forum'),
    url(r'^(?P<question_id>[0-9]+)/$', views.index1, name='index1')
]
