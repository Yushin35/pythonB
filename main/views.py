# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from django.shortcuts import render
from main.models import BlogPost
from django.template import loader, RequestContext
from django.shortcuts import render
from django.core.paginator import Paginator
# Create your views here.


def index1(request, question_id):
    """ posts = BlogPost.objects.all()
     context = {'posts': posts}
     return render(request, 'blog/index.html', context)"""
    posts = BlogPost.objects.all()
    t = loader.get_template('blog/index.html')
    context = {'posts': posts}
    #t.render(context)
    return HttpResponse(question_id)

def index(request):
    """ posts = BlogPost.objects.all()
     context = {'posts': posts}
     return render(request, 'blog/index.html', context)"""
    index_html = loader.get_template('blog/main/index.html')
    return HttpResponse(index_html.render())


def blog(request):
    posts = BlogPost.objects.all()
    posts = Paginator(posts, 2)
    blog_index_html = loader.get_template('blog/blog/blog_index.html')
    context = {'posts': posts}
    return HttpResponse(blog_index_html.render(context))


def forum(request):
    forum_index_html = loader.get_template('blog/forum/forum_index.html')
    return HttpResponse(forum_index_html.render())